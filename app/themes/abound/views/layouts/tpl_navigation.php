<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
    <div class="container">
          <!-- Be sure to leave the brand out there if you want it shown -->
          <a class="brand" href="#">AcuityCoaching</a>
          
          <div class="nav-collapse">
			<?php $this->widget('zii.widgets.CMenu',array(
                    'htmlOptions'=>array('class'=>'pull-right nav'),
                    'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
					'itemCssClass'=>'item-test',
                    'encodeLabel'=>false,
                    'items'=>array(

						array('label'=>'Assignment', 'url'=>array('/assignment/admin'), 'visible'=>!Yii::app()->user->isGuest),
						array('label'=>'Coaches', 'url'=>array('/coach/admin'), 'visible'=>!Yii::app()->user->isGuest),
						array('label'=>'Clients', 'url'=>array('/client/admin'), 'visible'=>!Yii::app()->user->isGuest),

                        array('label'=>'Client Control<span class="caret"></span>', 
                        	'url'=>'/client/admin',
                        	'itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),
                        	'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"), 
                        'items'=>array(
	                        array('label'=>'Sponsers', 'url'=>array('/sponsor/admin')),
	                        array('label'=>'Coachee', 'url'=>array('/coachee/admin')),
	                        array('label'=>'Coachee Request', 'url'=>array('/coacheeRequest/admin')),
	                        array('label'=>'Coaching Contract', 'url'=>array('/coachingContract/admin' )),
	                      	array('label'=>'Coaching Contract Area', 'url'=>array('/coachingContractArea/admin' )),
							
						), 'visible'=>!Yii::app()->user->isGuest),
						
                        array('label'=>'Framework Control<span class="caret"></span>', 
                        	'url'=>'/framework/admin',
                        	'itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),
                        	'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"), 
                        'items'=>array(	
	                        array('label'=>'Framework', 'url'=>array('/framework/admin')),
	                        array('label'=>'Assignment Status', 'url'=>array('/assignStatus/admin')),
	                        array('label'=>'Coaching Contract Sections', 'url'=>array('/coachingContractSection/admin' )),
	                        array('label'=>'Coaching Contract Area', 'url'=>array('/coachingContractArea/admin' )),
	                        array('label'=>'Coaching Contract Data Gathering', 'url'=>array('/coachingContractDataGathering/admin')),
	                        array('label'=>'Client Coachee Levels', 'url'=>array('/level/admin')),
	                        array('label'=>'Competence', 'url'=>array('/competence/admin')),
							
						), 'visible'=>!Yii::app()->user->isGuest),
						
						//array('label'=>'Chart', 'url'=>array('/site/graph'), 'visible'=>!Yii::app()->user->isGuest),
						//array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
                        array('label'=>'Admin <span class="caret"></span>', 
                        	'url'=>'/site/admin',
                        	'itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),
                        	'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"), 
                        'items'=>array(
	                        array('label'=>'User', 'url'=>array('/user/admin')),
	                        array('label'=>'Address', 'url'=>array('/address/admin')),
	                        array('label'=>'Coaching Objectives', 'url'=>array('/coachingObjectives/admin' )),
                        ), 'visible'=>!Yii::app()->user->isGuest ),
					
                        array('label'=>'Theme <span class="caret"></span>', 'url'=>'#','itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"), 
                        'items'=>array(
	                        array('label'=>'Dashboard', 'url'=>array('/site/index')),
	                        array('label'=>'Graphs & Charts', 'url'=>array('/site/page', 'view'=>'graphs')),
	                        array('label'=>'Forms', 'url'=>array('/site/page', 'view'=>'forms')),
	                        array('label'=>'Tables', 'url'=>array('/site/page', 'view'=>'tables')),
							array('label'=>'Interface', 'url'=>array('/site/page', 'view'=>'interface')),
	                        array('label'=>'Typography', 'url'=>array('/site/page', 'view'=>'typography')),
                        )),
                        /*array('label'=>'Gii generated', 'url'=>array('customer/index')),*/
                        array('label'=>'My Account <span class="caret"></span>', 'url'=>'#','itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"), 
                        'items'=>array(
                            array('label'=>'My Messages <span class="badge badge-warning pull-right">26</span>', 'url'=>'#'),
							array('label'=>'My Tasks <span class="badge badge-important pull-right">112</span>', 'url'=>'#'),
							array('label'=>'My Invoices <span class="badge badge-info pull-right">12</span>', 'url'=>'#'),
							array('label'=>'Separated link', 'url'=>'#'),
							array('label'=>'One more separated link', 'url'=>'#'),
                        )),
                        array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                        array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
                    ),
                )); ?>
    	</div>
    </div>
	</div>
</div>

<div class="subnav navbar navbar-fixed-top">
    <div class="navbar-inner">
    	<div class="container">
           <form class="navbar-search pull-right" action="">
           <input type="text" class="search-query span2" placeholder="Search">
           
           </form>
    	</div>
    	<!-- container -->
    </div><!-- navbar-inner -->
</div><!-- subnav -->