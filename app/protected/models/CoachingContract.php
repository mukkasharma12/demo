<?php

Yii::import('application.models._base.BaseCoachingContract');

class CoachingContract extends BaseCoachingContract
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	
	public function __toString(){
		return	$this->coaching_contract_id;
	}
}