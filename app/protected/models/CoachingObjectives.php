<?php

Yii::import('application.models._base.BaseCoachingObjectives');

class CoachingObjectives extends BaseCoachingObjectives
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	
	public function __toString() {
		$string = "Objective ";
		$string = $string.' '.$this->coaching_objectives_id;
		
		$competence = Competence::model()->findByPk($this->competence_id);
		
		$string = $string.' '.GxHtml::valueEx($competence,'name');
		
		return $string;
	}
	
}