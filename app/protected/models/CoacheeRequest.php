<?php

Yii::import('application.models._base.BaseCoacheeRequest');

class CoacheeRequest extends BaseCoacheeRequest
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}