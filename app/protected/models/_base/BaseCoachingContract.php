<?php

/**
 * This is the model base class for the table "coaching_contract".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "CoachingContract".
 *
 * Columns in table "coaching_contract" available as properties of the model,
 * followed by relations of table "coaching_contract" available as properties of the model.
 *
 * @property integer $coaching_contract_id
 * @property integer $client_id
 * @property integer $assignment_id
 * @property integer $framework_id
 * @property integer $coach_id
 * @property integer $sponsor_id
 * @property integer $manager_id
 * @property string $manager_present
 * @property string $sponsor_present
 * @property string $initial_meeting_date
 * @property integer $number_of_sessions
 * @property integer $data_gathering_id
 * @property string $review_point
 * @property string $record_created
 * @property string $record_updated
 * @property integer $created_by
 *
 * @property User $createdBy
 * @property Coach $coach
 * @property Sponsor $sponsor
 * @property Framework $framework
 * @property CoachingContractDataGathering $dataGathering
 * @property Assignment $assignment
 * @property Client $client
 * @property CoachingContractArea[] $coachingContractAreas
 */
abstract class BaseCoachingContract extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'coaching_contract';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'CoachingContract|CoachingContracts', $n);
	}

	public static function representingColumn() {
		return 'manager_present';
	}

	public function rules() {
		return array(
			array('client_id, assignment_id, framework_id, coach_id', 'required'),
			array('client_id, assignment_id, framework_id, coach_id, sponsor_id, manager_id, number_of_sessions, data_gathering_id, created_by', 'numerical', 'integerOnly'=>true),
			array('manager_present, sponsor_present', 'length', 'max'=>1),
			array('review_point', 'length', 'max'=>255),
			array('initial_meeting_date, record_created, record_updated', 'safe'),
			array('sponsor_id, manager_id, manager_present, sponsor_present, initial_meeting_date, number_of_sessions, data_gathering_id, review_point, record_created, record_updated, created_by', 'default', 'setOnEmpty' => true, 'value' => null),
			array('coaching_contract_id, client_id, assignment_id, framework_id, coach_id, sponsor_id, manager_id, manager_present, sponsor_present, initial_meeting_date, number_of_sessions, data_gathering_id, review_point, record_created, record_updated, created_by', 'safe', 'on'=>'search'),
		);
	}

	public function beforeSave() {
		if ($this->isNewRecord){
        		$this->record_created = new CDbExpression('NOW()');
		}else{
	        $this->record_updated = new CDbExpression('NOW()');		
		}
    	return parent::beforeSave();
	}

	public function relations() {
		return array(
			'createdBy' => array(self::BELONGS_TO, 'User', 'created_by'),
			'coach' => array(self::BELONGS_TO, 'Coach', 'coach_id'),
			'sponsor' => array(self::BELONGS_TO, 'Sponsor', 'sponsor_id'),
			'framework' => array(self::BELONGS_TO, 'Framework', 'framework_id'),
			'dataGathering' => array(self::BELONGS_TO, 'CoachingContractDataGathering', 'data_gathering_id'),
			'assignment' => array(self::BELONGS_TO, 'Assignment', 'assignment_id'),
			'client' => array(self::BELONGS_TO, 'Client', 'client_id'),
			'coachingContractAreas' => array(self::HAS_MANY, 'CoachingContractArea', 'contract_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'coaching_contract_id' => Yii::t('app', 'Coaching Contract'),
			'client_id' => null,
			'assignment_id' => null,
			'framework_id' => null,
			'coach_id' => null,
			'sponsor_id' => null,
			'manager_id' => Yii::t('app', 'Manager'),
			'manager_present' => Yii::t('app', 'Manager Present'),
			'sponsor_present' => Yii::t('app', 'Sponsor Present'),
			'initial_meeting_date' => Yii::t('app', 'Initial Meeting Date'),
			'number_of_sessions' => Yii::t('app', 'Number Of Sessions'),
			'data_gathering_id' => null,
			'review_point' => Yii::t('app', 'Review Point'),
			'record_created' => Yii::t('app', 'Record Created'),
			'record_updated' => Yii::t('app', 'Record Updated'),
			'created_by' => null,
			'createdBy' => null,
			'coach' => null,
			'sponsor' => null,
			'framework' => null,
			'dataGathering' => null,
			'assignment' => null,
			'client' => null,
			'coachingContractAreas' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('coaching_contract_id', $this->coaching_contract_id);
		$criteria->compare('client_id', $this->client_id);
		$criteria->compare('assignment_id', $this->assignment_id);
		$criteria->compare('framework_id', $this->framework_id);
		$criteria->compare('coach_id', $this->coach_id);
		$criteria->compare('sponsor_id', $this->sponsor_id);
		$criteria->compare('manager_id', $this->manager_id);
		$criteria->compare('manager_present', $this->manager_present, true);
		$criteria->compare('sponsor_present', $this->sponsor_present, true);
		$criteria->compare('initial_meeting_date', $this->initial_meeting_date, true);
		$criteria->compare('number_of_sessions', $this->number_of_sessions);
		$criteria->compare('data_gathering_id', $this->data_gathering_id);
		$criteria->compare('review_point', $this->review_point, true);
		$criteria->compare('record_created', $this->record_created, true);
		$criteria->compare('record_updated', $this->record_updated, true);
		$criteria->compare('created_by', $this->created_by);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}