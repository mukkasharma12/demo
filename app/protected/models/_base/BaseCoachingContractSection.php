<?php

/**
 * This is the model base class for the table "coaching_contract_section".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "CoachingContractSection".
 *
 * Columns in table "coaching_contract_section" available as properties of the model,
 * followed by relations of table "coaching_contract_section" available as properties of the model.
 *
 * @property integer $section_id
 * @property integer $framework_id
 * @property string $heading
 * @property string $summary
 * @property string $active
 * @property string $record_created
 * @property string $record_updated
 * @property integer $created_by
 *
 * @property CoachingContractArea[] $coachingContractAreas
 * @property User $createdBy
 * @property Framework $framework
 */
abstract class BaseCoachingContractSection extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'coaching_contract_section';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'CoachingContractSection|CoachingContractSections', $n);
	}

	public static function representingColumn() {
		return 'heading';
	}

	public function rules() {
		return array(
			array('heading', 'required'),
			array('framework_id, created_by', 'numerical', 'integerOnly'=>true),
			array('heading', 'length', 'max'=>128),
			array('active', 'length', 'max'=>1),
			array('summary, record_created, record_updated', 'safe'),
			array('framework_id, summary, active, record_created, record_updated, created_by', 'default', 'setOnEmpty' => true, 'value' => null),
			array('section_id, framework_id, heading, summary, active, record_created, record_updated, created_by', 'safe', 'on'=>'search'),
		);
	}

	public function beforeSave() {
		if ($this->isNewRecord){
        		$this->record_created = new CDbExpression('NOW()');
		}else{
	        $this->record_updated = new CDbExpression('NOW()');		
		}
    	return parent::beforeSave();
	}

	public function relations() {
		return array(
			'coachingContractAreas' => array(self::HAS_MANY, 'CoachingContractArea', 'section_id'),
			'createdBy' => array(self::BELONGS_TO, 'User', 'created_by'),
			'framework' => array(self::BELONGS_TO, 'Framework', 'framework_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'section_id' => Yii::t('app', 'Section'),
			'framework_id' => null,
			'heading' => Yii::t('app', 'Heading'),
			'summary' => Yii::t('app', 'Summary'),
			'active' => Yii::t('app', 'Active'),
			'record_created' => Yii::t('app', 'Record Created'),
			'record_updated' => Yii::t('app', 'Record Updated'),
			'created_by' => null,
			'coachingContractAreas' => null,
			'createdBy' => null,
			'framework' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('section_id', $this->section_id);
		$criteria->compare('framework_id', $this->framework_id);
		$criteria->compare('heading', $this->heading, true);
		$criteria->compare('summary', $this->summary, true);
		$criteria->compare('active', $this->active, true);
		$criteria->compare('record_created', $this->record_created, true);
		$criteria->compare('record_updated', $this->record_updated, true);
		$criteria->compare('created_by', $this->created_by);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}