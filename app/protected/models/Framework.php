<?php

Yii::import('application.models._base.BaseFramework');

class Framework extends BaseFramework
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}