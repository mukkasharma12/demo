<?php

Yii::import('application.models._base.BaseCoachingContractSection');

class CoachingContractSection extends BaseCoachingContractSection
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}