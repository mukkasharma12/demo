<?php

class CoachingContractSectionController extends GxController {

public function actionadd_area_form($fId,$sId)
{
$model = new CoachingContractSection;
	if (isset($_REQUEST['fId']) && isset($_REQUEST['sId'])) 
	{
	
		$this->render('addArea', array( 'model' => $model,'fId'=>$_REQUEST['fId'],'sId'=>$_REQUEST['sId']));
		
	}
	
}




	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'CoachingContractSection'),
		));
	}

	
	public function actionCreate() {
		$model = new CoachingContractSection;
	 	if (isset($_POST['CoachingContractSection'])) {
			$model->setAttributes($_POST['CoachingContractSection']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->section_id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'CoachingContractSection');

		
		if (isset($_POST['CoachingContractSection'])) {
			$model->setAttributes($_POST['CoachingContractSection']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->section_id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'CoachingContractSection')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('CoachingContractSection');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new CoachingContractSection('search');
		$model->unsetAttributes();

		if (isset($_GET['CoachingContractSection']))
			$model->setAttributes($_GET['CoachingContractSection']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}

