<?php

class CoacheeController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Coachee'),
		));
	}

	public function actionCreate() {
		$model = new Coachee;


		if (isset($_POST['Coachee'])) {
			$model->setAttributes($_POST['Coachee']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->coachee_id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Coachee');


		if (isset($_POST['Coachee'])) {
			$model->setAttributes($_POST['Coachee']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->coachee_id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Coachee')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Coachee');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Coachee('search');
		$model->unsetAttributes();

		if (isset($_GET['Coachee']))
			$model->setAttributes($_GET['Coachee']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}