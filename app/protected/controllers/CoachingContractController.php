<?php

class CoachingContractController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'CoachingContract'),
		));
	}

	public function actionCreate() {
		$model = new CoachingContract;


		if (isset($_POST['CoachingContract'])) {
			$model->setAttributes($_POST['CoachingContract']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->coaching_contract_id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'CoachingContract');


		if (isset($_POST['CoachingContract'])) {
			$model->setAttributes($_POST['CoachingContract']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->coaching_contract_id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'CoachingContract')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('CoachingContract');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new CoachingContract('search');
		$model->unsetAttributes();

		if (isset($_GET['CoachingContract']))
			$model->setAttributes($_GET['CoachingContract']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}