<?php

class AssignStatusController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'AssignStatus'),
		));
	}

	public function actionCreate() {
		$model = new AssignStatus;


		if (isset($_POST['AssignStatus'])) {
			$model->setAttributes($_POST['AssignStatus']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->assign_status_id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'AssignStatus');


		if (isset($_POST['AssignStatus'])) {
			$model->setAttributes($_POST['AssignStatus']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->assign_status_id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'AssignStatus')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('AssignStatus');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new AssignStatus('search');
		$model->unsetAttributes();

		if (isset($_GET['AssignStatus']))
			$model->setAttributes($_GET['AssignStatus']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}