<?php

class CoachingObjectivesController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'CoachingObjectives'),
		));
	}

	public function actionCreate() {
		$model = new CoachingObjectives;


		if (isset($_POST['CoachingObjectives'])) {
			$model->setAttributes($_POST['CoachingObjectives']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->coaching_objectives_id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}
	
	public function actionAddNew(){
		$model = new CoachingObjectives;
		
		$flag = true;
		if(isset($_POST['CoachingObjectives']))
        {
        	$flag = false;
			$model->setAttributes($_POST['CoachingObjectives']);
			
            if($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					//Yii::app()->end();
					$this->redirect(array('Assignment/view', 'id' => $model->assignment_id));
			}
		}

	    if($flag) {
			if(isset($_GET['assignment_id']))
			{
				$model->assignment_id=$_GET['assignment_id'];
			}

	        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
	        $this->renderPartial('popupForm',array('model'=>$model),false,true);
	    }
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'CoachingObjectives');


		if (isset($_POST['CoachingObjectives'])) {
			$model->setAttributes($_POST['CoachingObjectives']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->coaching_objectives_id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'CoachingObjectives')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('CoachingObjectives');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new CoachingObjectives('search');
		$model->unsetAttributes();

		if (isset($_GET['CoachingObjectives']))
			$model->setAttributes($_GET['CoachingObjectives']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}