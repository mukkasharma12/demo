<?php

class CoachController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Coach'),
		));
	}

	public function actionCreate() {
		$model = new Coach;


		if (isset($_POST['Coach'])) {
			$model->setAttributes($_POST['Coach']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->coach_id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Coach');


		if (isset($_POST['Coach'])) {
			$model->setAttributes($_POST['Coach']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->coach_id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Coach')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Coach');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Coach('search');
		$model->unsetAttributes();

		if (isset($_GET['Coach']))
			$model->setAttributes($_GET['Coach']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}