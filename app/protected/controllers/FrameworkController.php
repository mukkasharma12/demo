<?php
class FrameworkController extends GxController {
public function actionView($id) {
$this->render('view', array(
			'model' => $this->loadModel($id, 'Framework'),
			
		));
	}
public function actionAddCompetence($id) {
	$param = array('framework_id'=>$id);
	$model = Framework::model()->findByAttributes($param); 
	$model=new Competence;
	$model->framework_id=$id;
	$model->name=$_POST['competence'];
	$model->save();
	$this->redirect(array('update', 'id' =>$id));
	}
	// Add contract section...
	
	public function actionAddContract($id) {
	$id;
	$param = array('framework_id'=>$id);
	$model=new CoachingContractSection;
	$model->framework_id=$id;	
	$model->heading=$_POST['contract'];			
	$model->summary=$_POST['summary'];	
	$model->save();		
	$this->redirect(array('update', 'id' =>$id));
	}
//Create  Framework 

	public function actionCreate() {
		$model = new Framework;
		
		if (isset($_POST['Framework'])) {
			$model->setAttributes($_POST['Framework']);

			if ($model->save()) {
				$sql="select max(framework_id) as framework_id from  framework";
				$command=Yii::app()->db->createCommand($sql);
				$rows=$command->queryAll();
				foreach($rows as $row)
				{
				$data=$row['framework_id'];
				}
			   $this->redirect(array('update', 'id' => $model->framework_id));
			}
			}
		

		$this->render('create', array( 'model' => $model));
	}
	
	
	//
	
	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Framework');


		if (isset($_POST['Framework'])) {
			$model->setAttributes($_POST['Framework']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->framework_id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
	echo $id;	
	echo $this->actionActiveDisable($id);
	exit;
	$command = Yii::app()->db->createCommand();
					$command->update('framework', array(
					'active'=>'N',
					), 'framework_id=:id', array(':id'=>$id));
          			 $this->redirect(array('admin'));
				
	
		if (Yii::app()->getRequest()->getIsPostRequest()) {
		$this->loadModel($id, 'Framework')->delete();

		if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Framework');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {

		$model = new Framework('search');
		
		$model->unsetAttributes();
		
		if (isset($_GET['Framework']))
	
			$model->setAttributes($_GET['Framework']);
			$this->render('admin', array(
			'model' => $model,
		));
	}
	
	public function actiondeleteOneCompetence() {
		$competenceid=$_REQUEST['competence_id'];
		$id=$_REQUEST['id'];
		$this->loadModel($competenceid, 'Competence')->delete();
		$this->redirect(array('update', 'id' => $id));
	}
	
		public function actiondeleteOneContractSection($id) {
		$id=$_REQUEST['id'];
		$contractsectionid=$_REQUEST['contract_section_id'];
		$this->loadModel($contractsectionid, 'CoachingContractSection')->delete();
		$this->redirect(array('update', 'id' => $id));
	}
	public function actionActiveDisable($id)
	{
	echo $id;
	$model = Framework::model()->findByPk($id);
	$model->active = 'N';
    $model->save();
	$this->redirect(array('admin'));
					 
}
	
	public function actionCreateAll() {
		$competence = new Competence;

		if (isset($_POST['Competence'])) {
			$competence->setAttributes($_POST['Competence']);

			if ($competence->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
//				else
//					$this->redirect(array('view', 'id' => $competence->competence_id));
			}
		}
		$framework = new Framework;
		if (isset($_POST['Framework'])) {
			$framework->setAttributes($_POST['Framework']);

			if ($framework->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
//				else
//					$this->redirect(array('view', 'id' => $model->framework_id));
			}
		}
		
		$coachingContractSection = new CoachingContractSection;
		if (isset($_POST['CoachingContractSection'])) {
			$coachingContractSection->setAttributes($_POST['CoachingContractSection']);
			if ($coachingContractSection->save()) 
			{
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				//else
//					$this->redirect(array('view', 'id' => $model->section_id));
			}
		}
				
		$this->render('createall', array(
			'framework' => $framework,
			'coachingContractSection' => $coachingContractSection,
			'competence' => $competence
		));
	}
}