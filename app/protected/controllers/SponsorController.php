<?php

class SponsorController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Sponsor'),
		));
	}

	public function actionCreate() {
		$model = new Sponsor;


		if (isset($_POST['Sponsor'])) {
			$model->setAttributes($_POST['Sponsor']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->sponsor_id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Sponsor');


		if (isset($_POST['Sponsor'])) {
			$model->setAttributes($_POST['Sponsor']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->sponsor_id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Sponsor')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Sponsor');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Sponsor('search');
		$model->unsetAttributes();

		if (isset($_GET['Sponsor']))
			$model->setAttributes($_GET['Sponsor']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}