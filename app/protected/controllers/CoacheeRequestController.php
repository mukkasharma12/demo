<?php

class CoacheeRequestController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'CoacheeRequest'),
		));
	}

	public function actionCreate() {
		$model = new CoacheeRequest;


		if (isset($_POST['CoacheeRequest'])) {
			$model->setAttributes($_POST['CoacheeRequest']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->coachee_request_id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'CoacheeRequest');


		if (isset($_POST['CoacheeRequest'])) {
			$model->setAttributes($_POST['CoacheeRequest']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->coachee_request_id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'CoacheeRequest')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('CoacheeRequest');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new CoacheeRequest('search');
		$model->unsetAttributes();

		if (isset($_GET['CoacheeRequest']))
			$model->setAttributes($_GET['CoacheeRequest']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}