<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Acuity Coaching',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'ext.giix.components.*', // giix components
	),

  'modules' => array(
  	'gii' => array(
  		'class' => 'system.gii.GiiModule',
			'password'=>'borris1',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
  		'generatorPaths' => array(
  			'ext.giix.generators', // giix generators
  		),
  	),
  ),
  
  'theme'=>'abound',
/*	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'borris1',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
	),
*/

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		// uncomment the following to enable URLs in path-format
		/*
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		 */
		/*
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		*/
		// uncomment the following to use a MySQL database
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=qtasksco_acuity',
			'emulatePrepare' => true,
			'username' => 'qtasksco_acuity',
			'password' => 'mN,~RA{(]0T%',
			'charset' => 'utf8',
		),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		
				// ... google chrome logger extension
//		'log' => array(
//			'class' => 'CLogRouter',
//			'routes' => array(
//				array(
//					'class' => 'ext.phpconsole.PhpConsoleYiiExtension',
//					'handleErrors' => true,
//					'handleExceptions' => true,
//					'basePathToStrip' => $_SERVER['DOCUMENT_ROOT']
//				),
//			),
//		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning, info, trace',
					'categories'=>'system.*,application,vardump',
				),
				// uncomment the following to show log messages on web pages
				array(
					'class'=>'CWebLogRoute',
					'levels'=>'error, warning, info, trace',
					'categories'=>'application,vardump',
					'showInFireBug'=>true,
					'ignoreAjaxInFireBug'=>false,
				),
				/*array(
					'class' => 'ext.phpconsole.PhpConsoleYiiExtension',
					//'handleErrors' => true,
					//'handleExceptions' => true,
					'basePathToStrip' => $_SERVER['DOCUMENT_ROOT'],
					'categories'=>'chrome',
				),
				 */
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@qtasks.com',
	),
);