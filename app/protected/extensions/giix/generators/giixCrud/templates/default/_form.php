<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<div class="form">

<?php $ajax = ($this->enable_ajax_validation) ? 'true' : 'false'; ?>

<?php echo '<?php '; ?>
$form = $this->beginWidget('GxActiveForm', array(
	'id' => '<?php echo $this->class2id($this->modelClass); ?>-form',
	'enableAjaxValidation' => <?php echo $ajax; ?>,
));
<?php echo '?>'; ?>


	<p class="note">
		Fields with <span class="required">*</span> are required.
	</p>

	<?php echo "<?php echo \$form->errorSummary(\$model); ?>\n"; ?>

<?php foreach ($this->tableSchema->columns as $column): ?>
<?php if (!$column->autoIncrement):
		if (!(( $column->name == 'record_created') ||
		 	 ( $column->name == 'record_updated') ||
		 	 ( $column->name == 'active') ||
		 	 ( $column->name == 'created_by') ||
			 ( $column->name == 'user')  
			 ) ): ?>
		<div class="row">
		<?php echo "<?php echo " . $this->generateActiveLabel($this->modelClass, $column) . "; ?>\n"; ?>
		<?php echo "<?php " . $this->generateActiveField($this->modelClass, $column) . "; ?>\n"; ?>
		<?php echo "<?php echo \$form->error(\$model,'{$column->name}'); ?>\n"; ?>
		</div><!-- row -->
	<?php endif; ?>
<?php endif; ?>
<?php endforeach; ?>

<?php $active=false; 
	foreach ($this->getRelations($this->modelClass) as $relation):
	if ($active==true &&
		$relation[1] == GxActiveRecord::HAS_MANY ||
	   	$relation[1] == GxActiveRecord::MANY_MANY ): ?>
		<label> <?php echo '<?php'; ?> echo GxHtml::encode($model->getRelationLabel('<?php echo $relation[0]; ?>')); ?></label> 
		<?php echo '<?php ' . $this->generateActiveRelationField($this->modelClass, $relation) . "; ?>\n";
 	endif;
 endforeach;?> 

<?php echo "<?php
echo GxHtml::submitButton('Save');
\$this->endWidget();
?>\n"; ?>
</div><!-- form -->