<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'coaching_objectives_id'); ?>
		<?php echo $form->textField($model, 'coaching_objectives_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'competence_id'); ?>
		<?php echo $form->dropDownList($model, 'competence_id', GxHtml::listDataEx(Competence::model()->findAllAttributes(null, true)), array('prompt' => 'All')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'assignment_id'); ?>
		<?php echo $form->dropDownList($model, 'assignment_id', GxHtml::listDataEx(Assignment::model()->findAllAttributes(null, true)), array('prompt' => 'All')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'percent'); ?>
		<?php echo $form->textField($model, 'percent'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'notes'); ?>
		<?php echo $form->textArea($model, 'notes'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'active'); ?>
		<?php echo $form->textField($model, 'active', array('maxlength' => 1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'record_created'); ?>
		<?php echo $form->textField($model, 'record_created'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'record_updated'); ?>
		<?php echo $form->textField($model, 'record_updated'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'created_by'); ?>
		<?php echo $form->dropDownList($model, 'created_by', GxHtml::listDataEx(User::model()->findAllAttributes(null, true)), array('prompt' => 'All')); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
