<div class="form">
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
                'id'=>'popupDialog',
                'options'=>array(
                    'title'=>Yii::t('CoachingObjectives','Add Objective'),
                    'autoOpen'=>true,
                    'modal'=>'true',
                    'width'=>'auto',
                    'height'=>'auto',
                ),
                ));
echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php $this->endWidget('zii.widgets.jui.CJuiDialog');?>

</div> 
