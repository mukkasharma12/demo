<?php

$this->breadcrumbs = array(
	CoachingObjectives::label(2),
	'Index',
);

$this->menu = array(
	array('label'=>'Create' . ' ' . CoachingObjectives::label(), 'url' => array('create')),
	array('label'=>'Manage' . ' ' . CoachingObjectives::label(2), 'url' => array('admin')),
);
?>

<h1><?php echo GxHtml::encode(CoachingObjectives::label(2)); ?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 