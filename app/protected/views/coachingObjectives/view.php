<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	$model->coaching_objectives_id,
);

$this->menu=array(
	array('label'=>'List' . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>'Create' . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>'Update' . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->coaching_objectives_id)),
	array('label'=>'Delete' . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->coaching_objectives_id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage' . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo 'View' . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode($model->coaching_objectives_id); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'coaching_objectives_id',
array(
			'name' => 'competence',
			'type' => 'raw',
			'value' => $model->competence !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->competence)), array('competence/view', 'id' => GxActiveRecord::extractPkValue($model->competence, true))) : null,
			),
array(
			'name' => 'assignment',
			'type' => 'raw',
			'value' => $model->assignment !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->assignment)), array('assignment/view', 'id' => GxActiveRecord::extractPkValue($model->assignment, true))) : null,
			),
'percent',
'notes',
'active',
'record_created',
'record_updated',
array(
			'name' => 'createdBy',
			'type' => 'raw',
			'value' => $model->createdBy !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->createdBy)), array('user/view', 'id' => GxActiveRecord::extractPkValue($model->createdBy, true))) : null,
			),
	),
)); ?>

