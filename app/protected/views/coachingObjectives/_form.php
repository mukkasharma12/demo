<div class="form" id='coachingObjectiveForm'>


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'coaching-objectives-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		Fields with <span class="required">*</span> are required.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'competence_id'); ?>
		<?php
		$narrow_competence = null;
		if(isset($model->assignment_id)){
			$assignment = Assignment::model()->findByPk($model->assignment_id);
			if ( isset($assignment) ){
				$narrow_competence = array('framework_id'=>$assignment->framework_id);
				
				echo $form->dropDownList($model, 'competence_id', 
						GxHtml::listDataEx(
							Competence::model()->findAllByAttributes(
								$narrow_competence,
								true ) 
						), 
						array('empty'=>'') );
						
			}else{
				echo $form->dropDownList($model, 'competence_id', 
						GxHtml::listDataEx(
							Competence::model()->findAllAttributes(null, true )
						), 
						array('empty'=>'') );
			}
		}
		?>
		<?php echo $form->error($model,'competence_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php
		if (isset($model->assignment_id))
		{ 
			echo $form->hiddenField( $model, 'assignment_id', array('value'=>$model->assignment_id));
		}else{
			echo $form->labelEx($model,'assignment_id');
			echo $form->dropDownList($model, 'assignment_id', GxHtml::listDataEx(Assignment::model()->findAllAttributes(null, true)), array('empty'=>'') );
		}
		echo $form->error($model,'assignment_id');
		?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'percent'); ?>
		<?php echo $form->textField($model, 'percent'); ?>
		<?php echo $form->error($model,'percent'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'notes'); ?>
		<?php echo $form->textArea($model, 'notes'); ?>
		<?php echo $form->error($model,'notes'); ?>
		</div><!-- row -->
		
	 <div class="row buttons">
	 	
        <?php
        if (Yii::app()->getRequest()->getIsAjaxRequest()){  
	        echo CHtml::ajaxSubmitButton(Yii::t('CoachingObjectives','Add'),
	        			CHtml::normalizeUrl(array('coachingObjectives/AddNew','render'=>false)),
	        				array('success'=>'js: function(data) {
	                        	$("#popupDialog").dialog("close");
	                    }'),
	                    array('id'=>'closeObjectivesDialogue',
	                    			'class'=>'btn btn-primary')); 
        }else{
        	echo GxHtml::submitButton('Save');
        }?>
                    			
    </div>
 

<?php
$this->endWidget();
?>
</div><!-- form -->