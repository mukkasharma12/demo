<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('coaching_objectives_id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->coaching_objectives_id), array('view', 'id' => $data->coaching_objectives_id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('competence')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->competence)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('assignment')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->assignment)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('percent')); ?>:
	<?php echo GxHtml::encode($data->percent); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('notes')); ?>:
	<?php echo GxHtml::encode($data->notes); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('active')); ?>:
	<?php echo GxHtml::encode($data->active); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('record_created')); ?>:
	<?php echo GxHtml::encode($data->record_created); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('record_updated')); ?>:
	<?php echo GxHtml::encode($data->record_updated); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('createdBy')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->createdBy)); ?>
	<br />
	*/ ?>

</div>