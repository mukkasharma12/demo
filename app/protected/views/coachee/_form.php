<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'coachee-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		Fields with <span class="required">*</span> are required.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'client_id'); ?>
		<?php echo $form->dropDownList($model, 'client_id', GxHtml::listDataEx(Client::model()->findAllAttributes(null, true)), array('empty'=>'') ); ?>
		<?php echo $form->error($model,'client_id'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model, 'name', array('maxlength' => 128)); ?>
		<?php echo $form->error($model,'name'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model, 'email', array('maxlength' => 128)); ?>
		<?php echo $form->error($model,'email'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model, 'phone', array('maxlength' => 128)); ?>
		<?php echo $form->error($model,'phone'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'job_title'); ?>
		<?php echo $form->textField($model, 'job_title', array('maxlength' => 64)); ?>
		<?php echo $form->error($model,'job_title'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'location'); ?>
		<?php echo $form->textField($model, 'location', array('maxlength' => 64)); ?>
		<?php echo $form->error($model,'location'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'line_of_business'); ?>
		<?php echo $form->textField($model, 'line_of_business', array('maxlength' => 64)); ?>
		<?php echo $form->error($model,'line_of_business'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'address_id'); ?>
		<?php echo $form->dropDownList($model, 'address_id', GxHtml::listDataEx(Address::model()->findAllAttributes(null, true)), array('empty'=>'') ); ?>
		<?php echo $form->error($model,'address_id'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'sponsor_id'); ?>
		<?php echo $form->dropDownList($model, 'sponsor_id', GxHtml::listDataEx(Sponsor::model()->findAllAttributes(null, true)), array('empty'=>'') ); ?>
		<?php echo $form->error($model,'sponsor_id'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'levels_id'); ?>
		<?php echo $form->dropDownList($model, 'levels_id', GxHtml::listDataEx(Level::model()->findAllAttributes(null, true)), array('empty'=>'') ); ?>
		<?php echo $form->error($model,'levels_id'); ?>
		</div><!-- row -->
	
 
 <!--		<label> </label> -->
		   

<?php
echo GxHtml::submitButton('Save');
$this->endWidget();
?>
</div><!-- form -->