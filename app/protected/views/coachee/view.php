<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>'List' . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>'Create' . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>'Update' . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->coachee_id)),
	array('label'=>'Delete' . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->coachee_id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage' . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo 'View' . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'coachee_id',
array(
			'name' => 'client',
			'type' => 'raw',
			'value' => $model->client !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->client)), array('client/view', 'id' => GxActiveRecord::extractPkValue($model->client, true))) : null,
			),
'name',
'email',
'phone',
'job_title',
'location',
'line_of_business',
array(
			'name' => 'address',
			'type' => 'raw',
			'value' => $model->address !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->address)), array('address/view', 'id' => GxActiveRecord::extractPkValue($model->address, true))) : null,
			),
array(
			'name' => 'sponsor',
			'type' => 'raw',
			'value' => $model->sponsor !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->sponsor)), array('sponsor/view', 'id' => GxActiveRecord::extractPkValue($model->sponsor, true))) : null,
			),
array(
			'name' => 'levels',
			'type' => 'raw',
			'value' => $model->levels !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->levels)), array('level/view', 'id' => GxActiveRecord::extractPkValue($model->levels, true))) : null,
			),
'active',
'record_created',
'record_updated',
array(
			'name' => 'createdBy',
			'type' => 'raw',
			'value' => $model->createdBy !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->createdBy)), array('user/view', 'id' => GxActiveRecord::extractPkValue($model->createdBy, true))) : null,
			),
	),
)); ?>

<h2><?php echo GxHtml::encode($model->getRelationLabel('assignments')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->assignments as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('assignment/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?>