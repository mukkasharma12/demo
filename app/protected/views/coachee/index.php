<?php

$this->breadcrumbs = array(
	Coachee::label(2),
	'Index',
);

$this->menu = array(
	array('label'=>'Create' . ' ' . Coachee::label(), 'url' => array('create')),
	array('label'=>'Manage' . ' ' . Coachee::label(2), 'url' => array('admin')),
);
?>

<h1><?php echo GxHtml::encode(Coachee::label(2)); ?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 