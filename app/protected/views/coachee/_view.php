<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('coachee_id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->coachee_id), array('view', 'id' => $data->coachee_id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('client')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->client)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('name')); ?>:
	<?php echo GxHtml::encode($data->name); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('email')); ?>:
	<?php echo GxHtml::encode($data->email); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('phone')); ?>:
	<?php echo GxHtml::encode($data->phone); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('job_title')); ?>:
	<?php echo GxHtml::encode($data->job_title); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('location')); ?>:
	<?php echo GxHtml::encode($data->location); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('line_of_business')); ?>:
	<?php echo GxHtml::encode($data->line_of_business); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('address')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->address)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sponsor')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->sponsor)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('levels')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->levels)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('active')); ?>:
	<?php echo GxHtml::encode($data->active); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('record_created')); ?>:
	<?php echo GxHtml::encode($data->record_created); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('record_updated')); ?>:
	<?php echo GxHtml::encode($data->record_updated); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('createdBy')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->createdBy)); ?>
	<br />
	*/ ?>

</div>