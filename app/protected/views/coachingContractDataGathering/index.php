<?php

$this->breadcrumbs = array(
	CoachingContractDataGathering::label(2),
	'Index',
);

$this->menu = array(
	array('label'=>'Create' . ' ' . CoachingContractDataGathering::label(), 'url' => array('create')),
	array('label'=>'Manage' . ' ' . CoachingContractDataGathering::label(2), 'url' => array('admin')),
);
?>

<h1><?php echo GxHtml::encode(CoachingContractDataGathering::label(2)); ?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 