<?php

$this->breadcrumbs = array(
	Competence::label(2),
	'Index',
);

$this->menu = array(
	array('label'=>'Create' . ' ' . Competence::label(), 'url' => array('create')),
	array('label'=>'Manage' . ' ' . Competence::label(2), 'url' => array('admin')),
);
?>

<h1><?php echo GxHtml::encode(Competence::label(2)); ?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 