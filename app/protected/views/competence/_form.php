<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'competence-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		Fields with <span class="required">*</span> are required.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'framework_id'); ?>
		<?php echo $form->dropDownList($model, 'framework_id', GxHtml::listDataEx(Framework::model()->findAllAttributes(null, true)), array('empty'=>'') ); ?>
		<?php echo $form->error($model,'framework_id'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model, 'name', array('maxlength' => 127)); ?>
		<?php echo $form->error($model,'name'); ?>
		</div><!-- row -->
	
 
 <!--		<label> </label> -->
		   

<?php
echo GxHtml::submitButton('Save');
$this->endWidget();
?>
</div><!-- form -->