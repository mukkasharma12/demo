<?php

$this->breadcrumbs = array(
	Coach::label(2),
	'Index',
);

$this->menu = array(
	array('label'=>'Create' . ' ' . Coach::label(), 'url' => array('create')),
	array('label'=>'Manage' . ' ' . Coach::label(2), 'url' => array('admin')),
);
?>

<h1><?php echo GxHtml::encode(Coach::label(2)); ?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 