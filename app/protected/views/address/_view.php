<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('address_id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->address_id), array('view', 'id' => $data->address_id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('address_line_1')); ?>:
	<?php echo GxHtml::encode($data->address_line_1); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('address_line_2')); ?>:
	<?php echo GxHtml::encode($data->address_line_2); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('address_line_3')); ?>:
	<?php echo GxHtml::encode($data->address_line_3); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('city')); ?>:
	<?php echo GxHtml::encode($data->city); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('country')); ?>:
	<?php echo GxHtml::encode($data->country); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('postcode')); ?>:
	<?php echo GxHtml::encode($data->postcode); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('active')); ?>:
	<?php echo GxHtml::encode($data->active); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('record_created')); ?>:
	<?php echo GxHtml::encode($data->record_created); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('record_updated')); ?>:
	<?php echo GxHtml::encode($data->record_updated); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('createdBy')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->createdBy)); ?>
	<br />
	*/ ?>

</div>