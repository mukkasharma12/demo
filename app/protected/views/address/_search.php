<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'address_id'); ?>
		<?php echo $form->textField($model, 'address_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'address_line_1'); ?>
		<?php echo $form->textArea($model, 'address_line_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'address_line_2'); ?>
		<?php echo $form->textArea($model, 'address_line_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'address_line_3'); ?>
		<?php echo $form->textArea($model, 'address_line_3'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'city'); ?>
		<?php echo $form->textArea($model, 'city'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'country'); ?>
		<?php echo $form->textArea($model, 'country'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'postcode'); ?>
		<?php echo $form->textArea($model, 'postcode'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'active'); ?>
		<?php echo $form->textField($model, 'active', array('maxlength' => 1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'record_created'); ?>
		<?php echo $form->textField($model, 'record_created'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'record_updated'); ?>
		<?php echo $form->textField($model, 'record_updated'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'created_by'); ?>
		<?php echo $form->dropDownList($model, 'created_by', GxHtml::listDataEx(User::model()->findAllAttributes(null, true)), array('prompt' => 'All')); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
