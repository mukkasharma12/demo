<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'address-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		Fields with <span class="required">*</span> are required.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'address_line_1'); ?>
		<?php echo $form->textArea($model, 'address_line_1'); ?>
		<?php echo $form->error($model,'address_line_1'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'address_line_2'); ?>
		<?php echo $form->textArea($model, 'address_line_2'); ?>
		<?php echo $form->error($model,'address_line_2'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'address_line_3'); ?>
		<?php echo $form->textArea($model, 'address_line_3'); ?>
		<?php echo $form->error($model,'address_line_3'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'city'); ?>
		<?php echo $form->textArea($model, 'city'); ?>
		<?php echo $form->error($model,'city'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'country'); ?>
		<?php echo $form->textArea($model, 'country'); ?>
		<?php echo $form->error($model,'country'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'postcode'); ?>
		<?php echo $form->textArea($model, 'postcode'); ?>
		<?php echo $form->error($model,'postcode'); ?>
		</div><!-- row -->
	
 
 <!--		<label> </label> -->
		   

<?php
echo GxHtml::submitButton('Save');
$this->endWidget();
?>
</div><!-- form -->