<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('area_id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->area_id), array('view', 'id' => $data->area_id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('section')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->section)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('contract')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->contract)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('summary')); ?>:
	<?php echo GxHtml::encode($data->summary); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('note')); ?>:
	<?php echo GxHtml::encode($data->note); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('impact_on_business')); ?>:
	<?php echo GxHtml::encode($data->impact_on_business); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('competence')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->competence)); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('record_created')); ?>:
	<?php echo GxHtml::encode($data->record_created); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('record_updated')); ?>:
	<?php echo GxHtml::encode($data->record_updated); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('createdBy')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->createdBy)); ?>
	<br />
	*/ ?>

</div>