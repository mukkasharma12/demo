<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'area_id'); ?>
		<?php echo $form->textField($model, 'area_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'section_id'); ?>
		<?php echo $form->dropDownList($model, 'section_id', GxHtml::listDataEx(CoachingContractSection::model()->findAllAttributes(null, true)), array('prompt' => 'All')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'contract_id'); ?>
		<?php echo $form->dropDownList($model, 'contract_id', GxHtml::listDataEx(CoachingContract::model()->findAllAttributes(null, true)), array('prompt' => 'All')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'summary'); ?>
		<?php echo $form->textArea($model, 'summary'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'note'); ?>
		<?php echo $form->textArea($model, 'note'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'impact_on_business'); ?>
		<?php echo $form->textArea($model, 'impact_on_business'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'competence_id'); ?>
		<?php echo $form->dropDownList($model, 'competence_id', GxHtml::listDataEx(Competence::model()->findAllAttributes(null, true)), array('prompt' => 'All')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'record_created'); ?>
		<?php echo $form->textField($model, 'record_created'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'record_updated'); ?>
		<?php echo $form->textField($model, 'record_updated'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'created_by'); ?>
		<?php echo $form->dropDownList($model, 'created_by', GxHtml::listDataEx(User::model()->findAllAttributes(null, true)), array('prompt' => 'All')); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
