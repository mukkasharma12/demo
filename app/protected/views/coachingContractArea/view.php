<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>'List' . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>'Create' . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>'Update' . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->area_id)),
	array('label'=>'Delete' . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->area_id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage' . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo 'View' . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'area_id',
array(
			'name' => 'section',
			'type' => 'raw',
			'value' => $model->section !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->section)), array('coachingContractSection/view', 'id' => GxActiveRecord::extractPkValue($model->section, true))) : null,
			),
array(
			'name' => 'contract',
			'type' => 'raw',
			'value' => $model->contract !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->contract)), array('coachingContract/view', 'id' => GxActiveRecord::extractPkValue($model->contract, true))) : null,
			),
'summary',
'note',
'impact_on_business',
array(
			'name' => 'competence',
			'type' => 'raw',
			'value' => $model->competence !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->competence)), array('competence/view', 'id' => GxActiveRecord::extractPkValue($model->competence, true))) : null,
			),
'record_created',
'record_updated',
array(
			'name' => 'createdBy',
			'type' => 'raw',
			'value' => $model->createdBy !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->createdBy)), array('user/view', 'id' => GxActiveRecord::extractPkValue($model->createdBy, true))) : null,
			),
	),
)); ?>

