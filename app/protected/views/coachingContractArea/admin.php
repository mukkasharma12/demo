<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	'Manage',
);

$this->menu = array(
		array('label'=>'List' . ' ' . $model->label(2), 'url'=>array('index')),
		array('label'=>'Create' . ' ' . $model->label(), 'url'=>array('create')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('coaching-contract-area-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo 'Manage' . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<p>
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<!-- <?php echo GxHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?> -->

<!-- <div class="search-form"> -->
	
<!-- </div> -->
<!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'coaching-contract-area-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'area_id',
		array(
				'name'=>'section_id',
				'value'=>'GxHtml::valueEx($data->section)',
				'filter'=>GxHtml::listDataEx(CoachingContractSection::model()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'contract_id',
				'value'=>'GxHtml::valueEx($data->contract)',
				'filter'=>GxHtml::listDataEx(CoachingContract::model()->findAllAttributes(null, true)),
				),
		'summary',
		'note',
		'impact_on_business',
		/*
		array(
				'name'=>'competence_id',
				'value'=>'GxHtml::valueEx($data->competence)',
				'filter'=>GxHtml::listDataEx(Competence::model()->findAllAttributes(null, true)),
				),
		'record_created',
		'record_updated',
		array(
				'name'=>'created_by',
				'value'=>'GxHtml::valueEx($data->createdBy)',
				'filter'=>GxHtml::listDataEx(User::model()->findAllAttributes(null, true)),
				),
		*/
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>