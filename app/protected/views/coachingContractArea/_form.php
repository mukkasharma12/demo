<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'coaching-contract-area-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		Fields with <span class="required">*</span> are required.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'section_id'); ?>
		<?php echo $form->dropDownList($model, 'section_id', GxHtml::listDataEx(CoachingContractSection::model()->findAllAttributes(null, true)), array('empty'=>'') ); ?>
		<?php echo $form->error($model,'section_id'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'contract_id'); ?>
		<?php echo $form->dropDownList($model, 'contract_id', GxHtml::listDataEx(CoachingContract::model()->findAllAttributes(null, true)), array('empty'=>'') ); ?>
		<?php echo $form->error($model,'contract_id'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'summary'); ?>
		<?php echo $form->textArea($model, 'summary'); ?>
		<?php echo $form->error($model,'summary'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'note'); ?>
		<?php echo $form->textArea($model, 'note'); ?>
		<?php echo $form->error($model,'note'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'impact_on_business'); ?>
		<?php echo $form->textArea($model, 'impact_on_business'); ?>
		<?php echo $form->error($model,'impact_on_business'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'competence_id'); ?>
		<?php echo $form->dropDownList($model, 'competence_id', GxHtml::listDataEx(Competence::model()->findAllAttributes(null, true)), array('empty'=>'') ); ?>
		<?php echo $form->error($model,'competence_id'); ?>
		</div><!-- row -->
	
 

<?php
echo GxHtml::submitButton('Save');
$this->endWidget();
?>
</div><!-- form -->