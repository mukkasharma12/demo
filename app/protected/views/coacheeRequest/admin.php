<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	'Manage',
);

$this->menu = array(
		array('label'=>'List' . ' ' . $model->label(2), 'url'=>array('index')),
		array('label'=>'Create' . ' ' . $model->label(), 'url'=>array('create')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('coachee-request-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo 'Manage' . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<p>
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<!-- <?php echo GxHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?> -->

<!-- <div class="search-form"> -->
	
<!-- </div> -->
<!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'coachee-request-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'coachee_request_id',
		array(
				'name'=>'client_id',
				'value'=>'GxHtml::valueEx($data->client)',
				'filter'=>GxHtml::listDataEx(Client::model()->findAllAttributes(null, true)),
				),
		'responsibilites',
		'training',
		array(
				'name'=>'prev_coach_id',
				'value'=>'GxHtml::valueEx($data->prevCoach)',
				'filter'=>GxHtml::listDataEx(Coach::model()->findAllAttributes(null, true)),
				),
		'prev_areas',
		/*
		'request_reason',
		'skills_to_dev',
		'challenges',
		'objectives',
		'spec_requirements',
		'coach_expectations',
		'number_of_sessions',
		'coaching_profiles',
		'record_created',
		'record_updated',
		array(
				'name'=>'created_by',
				'value'=>'GxHtml::valueEx($data->createdBy)',
				'filter'=>GxHtml::listDataEx(User::model()->findAllAttributes(null, true)),
				),
		*/
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>