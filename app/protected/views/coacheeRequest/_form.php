<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'coachee-request-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		Fields with <span class="required">*</span> are required.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'client_id'); ?>
		<?php echo $form->dropDownList($model, 'client_id', GxHtml::listDataEx(Client::model()->findAllAttributes(null, true)), array('empty'=>'') ); ?>
		<?php echo $form->error($model,'client_id'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'responsibilites'); ?>
		<?php echo $form->textField($model, 'responsibilites', array('maxlength' => 128)); ?>
		<?php echo $form->error($model,'responsibilites'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'training'); ?>
		<?php echo $form->textField($model, 'training', array('maxlength' => 128)); ?>
		<?php echo $form->error($model,'training'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'prev_coach_id'); ?>
		<?php echo $form->dropDownList($model, 'prev_coach_id', GxHtml::listDataEx(Coach::model()->findAllAttributes(null, true)), array('empty'=>'') ); ?>
		<?php echo $form->error($model,'prev_coach_id'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'prev_areas'); ?>
		<?php echo $form->textField($model, 'prev_areas', array('maxlength' => 128)); ?>
		<?php echo $form->error($model,'prev_areas'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'request_reason'); ?>
		<?php echo $form->textField($model, 'request_reason', array('maxlength' => 128)); ?>
		<?php echo $form->error($model,'request_reason'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'skills_to_dev'); ?>
		<?php echo $form->textField($model, 'skills_to_dev', array('maxlength' => 128)); ?>
		<?php echo $form->error($model,'skills_to_dev'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'challenges'); ?>
		<?php echo $form->textField($model, 'challenges', array('maxlength' => 128)); ?>
		<?php echo $form->error($model,'challenges'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'objectives'); ?>
		<?php echo $form->textField($model, 'objectives', array('maxlength' => 128)); ?>
		<?php echo $form->error($model,'objectives'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'spec_requirements'); ?>
		<?php echo $form->textField($model, 'spec_requirements', array('maxlength' => 128)); ?>
		<?php echo $form->error($model,'spec_requirements'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'coach_expectations'); ?>
		<?php echo $form->textField($model, 'coach_expectations', array('maxlength' => 128)); ?>
		<?php echo $form->error($model,'coach_expectations'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'number_of_sessions'); ?>
		<?php echo $form->textField($model, 'number_of_sessions', array('maxlength' => 128)); ?>
		<?php echo $form->error($model,'number_of_sessions'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'coaching_profiles'); ?>
		<?php echo $form->textField($model, 'coaching_profiles'); ?>
		<?php echo $form->error($model,'coaching_profiles'); ?>
		</div><!-- row -->
	
 
 <!--		<label> </label> -->
		   

<?php
echo GxHtml::submitButton('Save');
$this->endWidget();
?>
</div><!-- form -->