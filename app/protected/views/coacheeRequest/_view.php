<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('coachee_request_id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->coachee_request_id), array('view', 'id' => $data->coachee_request_id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('client')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->client)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('responsibilites')); ?>:
	<?php echo GxHtml::encode($data->responsibilites); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('training')); ?>:
	<?php echo GxHtml::encode($data->training); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('prevCoach')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->prevCoach)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('prev_areas')); ?>:
	<?php echo GxHtml::encode($data->prev_areas); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('request_reason')); ?>:
	<?php echo GxHtml::encode($data->request_reason); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('skills_to_dev')); ?>:
	<?php echo GxHtml::encode($data->skills_to_dev); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('challenges')); ?>:
	<?php echo GxHtml::encode($data->challenges); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('objectives')); ?>:
	<?php echo GxHtml::encode($data->objectives); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('spec_requirements')); ?>:
	<?php echo GxHtml::encode($data->spec_requirements); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('coach_expectations')); ?>:
	<?php echo GxHtml::encode($data->coach_expectations); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('number_of_sessions')); ?>:
	<?php echo GxHtml::encode($data->number_of_sessions); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('coaching_profiles')); ?>:
	<?php echo GxHtml::encode($data->coaching_profiles); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('record_created')); ?>:
	<?php echo GxHtml::encode($data->record_created); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('record_updated')); ?>:
	<?php echo GxHtml::encode($data->record_updated); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('createdBy')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->createdBy)); ?>
	<br />
	*/ ?>

</div>