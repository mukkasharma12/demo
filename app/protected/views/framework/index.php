<?php

$this->breadcrumbs = array(
	Framework::label(2),
	'Index',
);

$this->menu = array(
	array('label'=>'Create' . ' ' . Framework::label(), 'url' => array('create')),
	array('label'=>'Manage' . ' ' . Framework::label(2), 'url' => array('admin')),
);
?>

<h1><?php echo GxHtml::encode(Framework::label(2)); ?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 