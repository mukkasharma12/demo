<?php
$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	'Manage',
);


$this->menu = array(
		//array('label'=>'List' . ' ' . $model->label(2), 'url'=>array('index')),
		array('label'=>'Create' . ' ' . $model->label(), 'url'=>array('create')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('framework-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo 'Manage' . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<p>
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.

</p>


<!-- <?php echo GxHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?> -->

<!-- <div class="search-form"> -->
	
<!-- </div> -->
<!-- search-form -->
<?php


 $image=Yii::app()->request->getBaseUrl();
?>
<a href="<?php echo $image?>/index.php?r=framework/create"><img src="<?php echo $image?>/images/NEW.png" /></a>

<?php $this->widget('zii.widgets.grid.CGridView', array(
'htmlOptions'=>array('style'=>'cursor: pointer;'),
'selectionChanged'=>"function(id){window.location='" . Yii::app()->urlManager->createUrl('framework/update', array('id'=>'')) . "' + $.fn.yiiGridView.getSelection(id);}",
	'id' => 'framework-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
	  array(
		'name'=>'framework_id',
		'htmlOptions'=>array('width'=>'25%'),
		
		 ),
		'name',
		array(
		'name'=>'active',
		//header=>"Competences",
		),
		array('header' => 'competences', 'value' => 'count($data->competences)'),


		array('header' => 'coachingContractSections', 'value' => 'count($data->coachingContractSections)'),
		
		/*array(
				'name'=>'created_by',
				'value'=>'GxHtml::valueEx($data->createdBy)',
				'filter'=>GxHtml::listDataEx(User::model()->findAllAttributes(null, true)),
				),
				*/
		/*array(
			'class' => 'CButtonColumn',
		),*/
		
		array(
		'class'=>'CButtonColumn',
		'template'=>'{delete}',

		),
             
       
	
	),
	
));
?>
