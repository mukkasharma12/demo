<?php
 $id=$_REQUEST['id'];
$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);
$this->menu=array(
	array('label'=>'List' . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>'Create' . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>'Update' . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->framework_id)),
	array('label'=>'Delete' . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->framework_id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage' . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>
<h1><?php echo 'View' . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'framework_id',
'name',
'active',
'record_created',
'record_updated',
array(
			'name' => 'createdBy',
			'type' => 'raw',
			'value' => $model->createdBy !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->createdBy)), array('user/view', 'id' => GxActiveRecord::extractPkValue($model->createdBy, true))) : null,
			),
	),
)); ?>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->assignments as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('assignment/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->coachingContracts as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('coachingContract/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?>
<h2><?php echo GxHtml::encode($model->getRelationLabel('competences')); ?></h2>
 <?php
 $id=$_REQUEST['id'];
 $param = array('framework_id'=>$id);
$model123 = Competence::model()->findAllByAttributes($param); 
	$base=Yii::app()->getBaseUrl();	
		
?>
<table class="structure">
 <?php if($model123) {  ?>
		 <tr class="portlet-decoration">
		 <th>Competence name</th>
		 </tr>
        
		 <?php
		 	}
			foreach($model123 as $name)
		{
			echo "<tr>";
			echo "<td>";
			$compId=$name->attributes['competence_id'];
			echo "<a href='$base/index.php?r=competence/update&id=$compId'>". $comp=$name->attributes['name']. "</a>";
			
			echo "<input type='hidden' value='$comp' name='competence[]'>";
			echo "</td>";
			echo "<td>";
			echo CHtml::link(
    	'<i class="icon-trash icon-red"></i>',
    	 array('framework/deleteOneCompetence','competence_id'=>$compId,'id'=>$id)
    	
		);
			echo "</td>";
			echo "</tr>";
		}
		?>
      </table>	
	<br />
    <h2><?php echo GxHtml::encode($model->getRelationLabel('coachingContracts')); ?></h2>
        <br />
       <?php
		$param = array('framework_id'=>$id);
		$model123 = CoachingContractSection::model()->findAllByAttributes($param); 
	
		if($model123){
		
		?>
		<table class="structure">
		 <tr class="portlet-decoration">
		 <th>Contract Heading</th>
		 <th>Contract Summary</th>
		 </tr>
		 <?php
		 }
		foreach ($model123 as $name)
		{
			echo "<tr>";
			echo "<td>";
			$contId=$name->attributes['section_id'];
			echo  "<a href='$base/index.php?r=coachingContractSection/update&id=$contId'>". $name->attributes['heading']. "</a>";
			echo "</td>";
			echo "<td>";
			echo "<a href='$base/index.php?r=coachingContractSection/update&id=$contId'>" . $name->attributes['summary'] . "</a>";
			echo "</td>";
			echo "<td>";
			echo CHtml::link(
    	'<i class="icon-trash icon-red"></i>',
    	 array('framework/deleteOneContractSection','contract_section_id'=>$contId,'id'=>$id),
    	 array('confirm' => 'Are you sure?')
		 );
		    echo "</td>";
			echo "</tr>";
		}
		
		?>
		</table>