<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('user_id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->user_id), array('view', 'id' => $data->user_id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('name')); ?>:
	<?php echo GxHtml::encode($data->name); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('active')); ?>:
	<?php echo GxHtml::encode($data->active); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('record_created')); ?>:
	<?php echo GxHtml::encode($data->record_created); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('record_updated')); ?>:
	<?php echo GxHtml::encode($data->record_updated); ?>
	<br />

</div>