<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'coaching-contract-section-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		Fields with <span class="required">*</span> are required.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'framework_id'); ?>
		<?php echo $form->dropDownList($model, 'framework_id', GxHtml::listDataEx(Framework::model()->findAllAttributes(null, true)), array('empty'=>'') ); ?>
		<?php echo $form->error($model,'framework_id'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'heading'); ?>
		<?php echo $form->textField($model, 'heading', array('maxlength' => 128)); ?>
		<?php echo $form->error($model,'heading'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'summary'); ?>
		<?php echo $form->textArea($model, 'summary'); ?>
		<?php echo $form->error($model,'summary'); ?>
		</div><!-- row -->
	
 

<?php
echo GxHtml::submitButton('Save');
$this->endWidget();
?>
</div><!-- form -->