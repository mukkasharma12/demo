<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('section_id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->section_id), array('view', 'id' => $data->section_id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('framework')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->framework)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('heading')); ?>:
	<?php echo GxHtml::encode($data->heading); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('summary')); ?>:
	<?php echo GxHtml::encode($data->summary); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('active')); ?>:
	<?php echo GxHtml::encode($data->active); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('record_created')); ?>:
	<?php echo GxHtml::encode($data->record_created); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('record_updated')); ?>:
	<?php echo GxHtml::encode($data->record_updated); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('createdBy')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->createdBy)); ?>
	<br />
	*/ ?>

</div>