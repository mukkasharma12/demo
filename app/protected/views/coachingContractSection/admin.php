<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	'Manage',
);

$this->menu = array(
		array('label'=>'List' . ' ' . $model->label(2), 'url'=>array('index')),
		array('label'=>'Create' . ' ' . $model->label(), 'url'=>array('create')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('coaching-contract-section-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo 'Manage' . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<p>
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<!-- <?php echo GxHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?> -->

<!-- <div class="search-form"> -->
	
<!-- </div> -->
<!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'coaching-contract-section-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'section_id',
		array(
				'name'=>'framework_id',
				'value'=>'GxHtml::valueEx($data->framework)',
				'filter'=>GxHtml::listDataEx(Framework::model()->findAllAttributes(null, true)),
				),
		'heading',
		'summary',
		'active',
		'record_created',
		/*
		'record_updated',
		array(
				'name'=>'created_by',
				'value'=>'GxHtml::valueEx($data->createdBy)',
				'filter'=>GxHtml::listDataEx(User::model()->findAllAttributes(null, true)),
				),
		*/
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>