<?php

$this->breadcrumbs = array(
	Assignment::label(2),
	'Index',
);

$this->menu = array(
	array('label'=>'Create' . ' ' . Assignment::label(), 'url' => array('create')),
	array('label'=>'Manage' . ' ' . Assignment::label(2), 'url' => array('admin')),
);
?>

<h1><?php echo GxHtml::encode(Assignment::label(2)); ?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 