<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'assignment-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		Fields with <span class="required">*</span> are required.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'client_id'); ?>
		<?php echo $form->dropDownList($model, 'client_id', GxHtml::listDataEx(Client::model()->findAllAttributes(null, true)), array('empty'=>'') ); ?>
		<?php echo $form->error($model,'client_id'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'framework_id'); ?>
		<?php echo $form->dropDownList($model, 'framework_id', GxHtml::listDataEx(Framework::model()->findAllAttributes(null, true)), array('empty'=>'') ); ?>
		<?php echo $form->error($model,'framework_id'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'coachee_id'); ?>
		<?php echo $form->dropDownList($model, 'coachee_id', GxHtml::listDataEx(Coachee::model()->findAllAttributes(null, true)), array('empty'=>'') ); ?>
		<?php echo $form->error($model,'coachee_id'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'assign_status_id'); ?>
		<?php echo $form->dropDownList($model, 'assign_status_id', GxHtml::listDataEx(AssignStatus::model()->findAllAttributes(null, true)), array('empty'=>'') ); ?>
		<?php echo $form->error($model,'assign_status_id'); ?>
		</div><!-- row -->
	
 

<?php
echo GxHtml::submitButton('Save',array('class'=>'btn btn-success'));
$this->endWidget();
?>
</div><!-- form -->