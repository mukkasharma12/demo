<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('assignment_id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->assignment_id), array('view', 'id' => $data->assignment_id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('client')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->client)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('framework')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->framework)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('coachee')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->coachee)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('active')); ?>:
	<?php echo GxHtml::encode($data->active); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('assignStatus')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->assignStatus)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('record_created')); ?>:
	<?php echo GxHtml::encode($data->record_created); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('record_updated')); ?>:
	<?php echo GxHtml::encode($data->record_updated); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('createdBy')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->createdBy)); ?>
	<br />
	*/ ?>

</div>