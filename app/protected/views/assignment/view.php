<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>'List' . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>'Create' . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>'Update' . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->assignment_id)),
	array('label'=>'Delete' . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->assignment_id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage' . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo 'View' . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'assignment_id',
array(
			'name' => 'client',
			'type' => 'raw',
			'value' => $model->client !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->client)), array('client/view', 'id' => GxActiveRecord::extractPkValue($model->client, true))) : null,
			),
array(
			'name' => 'framework',
			'type' => 'raw',
			'value' => $model->framework !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->framework)), array('framework/view', 'id' => GxActiveRecord::extractPkValue($model->framework, true))) : null,
			),
array(
			'name' => 'coachee',
			'type' => 'raw',
			'value' => $model->coachee !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->coachee)), array('coachee/view', 'id' => GxActiveRecord::extractPkValue($model->coachee, true))) : null,
			),
'active',
array(
			'name' => 'assignStatus',
			'type' => 'raw',
			'value' => $model->assignStatus !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->assignStatus)), array('assignStatus/view', 'id' => GxActiveRecord::extractPkValue($model->assignStatus, true))) : null,
			),
'record_created',
'record_updated',
array(
			'name' => 'createdBy',
			'type' => 'raw',
			'value' => $model->createdBy !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->createdBy)), array('user/view', 'id' => GxActiveRecord::extractPkValue($model->createdBy, true))) : null,
			),
	),
)); ?>

<h2><?php echo GxHtml::encode($model->getRelationLabel('CoachingObjectives')); ?></h2>
<?php
	echo CHtml::ajaxButton ("Add Objective",
	                        $this->createUrl('CoachingObjectives/AddNew',array('assignment_id'=>$model->assignment_id)),
                      		array(
						        'onclick'=>'$("#popupDialog").dialog("open"); return false;',
						        'update'=>'#popupDialog'
						        ),
						        array('class'=>'btn btn-primary', 'id'=>'objectivesDialogue')
								);

?>

<div id="popupDialog"></div>

<?php
	echo GxHtml::openTag('ul');
	foreach($model->coachingObjectives as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('coachingObjectives/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?>

<h2><?php echo GxHtml::encode($model->getRelationLabel('coachingContracts')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->coachingContracts as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('coachingContract/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?>
	

