<?php
/* @var $this   SiteController */
/* @var $model  CFormModel */
/* @var $form   GraphForm */

$this->pageTitle=Yii::app()->name;

$this->breadcrumbs=array(
	'Chart',
);

?>

<h1><i>Acuity Oracle Test Page</i></h1>


<?php

$this->widget('ext.widgets.ddmenu.XDropDownMenu', array(
    'items'=>array(
        array('label'=>'Objectives','url'=>'#',
        	'items'=>array( array('label'=>'Objectives Achieved - Completed', 'url'=>array('/site/graph', 'chart'=>'1line'), 
        						'items'=>array( array('label'=>'Line Chart', 'url'=>array('/site/graph', 'chart'=>'1line')),
							            array('label'=>'Bar Vertical Chart', 'url'=>array('/site/graph', 'chart'=>'1barVertical')),
							            array('label'=>'Bar Horizontal Chart', 'url'=>array('/site/graph', 'chart'=>'1barHorizontal')),
		            					array('label'=>'Pie Chart', 'url'=>array('/site/graph', 'chart'=>'1pie')))
										),
				            array('label'=>'Objectives By Competence - All', 'url'=>array('/site/graph', 'chart'=>'2line'),
					        	'items'=>array( array('label'=>'Line Chart', 'url'=>array('/site/graph', 'chart'=>'2line')),
									            array('label'=>'Bar Vertical Chart', 'url'=>array('/site/graph', 'chart'=>'2barVertical')),
									            array('label'=>'Bar Horizontal Chart', 'url'=>array('/site/graph', 'chart'=>'2barHorizontal')),
									            array('label'=>'Pie Chart', 'url'=>array('/site/graph', 'chart'=>'2pie'))),
            							)
							)
    		)
		)
));

switch( $model->chartType )
{
	case 'line':
		$model->lineChart( $this );
		break;
	case 'barVertical':
		$model->barVerticalChart( $this );
		break;
	case 'barHorizontal':
		$model->barHorizontalChart( $this );
		break;
	case 'pie':
		$model->pieChart( $this );
		break;
}
	
?>

