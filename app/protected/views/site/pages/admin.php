<?php
/* @var $this SiteController */

$this->widget('ext.widgets.amenu.XActionMenu', array(
    'htmlOptions'=>array('class'=>'actionBar'),
    'items'=>array(
      
       array('label'=>Yii::t('ui', 'Address'), 'url'=>array('/address'), 'visible'=>!Yii::app()->user->isGuest),
       array('label'=>Yii::t('ui', 'Area'), 'url'=>array('/Area'), 'visible'=>!Yii::app()->user->isGuest),
       array('label'=>Yii::t('ui', 'AssignStatus'), 'url'=>array('/AssignStatus'), 'visible'=>!Yii::app()->user->isGuest),
       array('label'=>Yii::t('ui', 'Assignment'), 'url'=>array('/Assignment'), 'visible'=>!Yii::app()->user->isGuest),
       array('label'=>Yii::t('ui', 'Client'), 'url'=>array('/Client'), 'visible'=>!Yii::app()->user->isGuest),
       array('label'=>Yii::t('ui', 'Coachee'), 'url'=>array('/Coachee'), 'visible'=>!Yii::app()->user->isGuest),
       array('label'=>Yii::t('ui', 'CoacheeRequest'), 'url'=>array('/CoacheeRequest'), 'visible'=>!Yii::app()->user->isGuest),
       array('label'=>Yii::t('ui', 'CoachingContract'), 'url'=>array('/CoachingContract'), 'visible'=>!Yii::app()->user->isGuest),
       array('label'=>Yii::t('ui', 'CoachingObjectives'), 'url'=>array('/CoachingObjectives'), 'visible'=>!Yii::app()->user->isGuest),
       array('label'=>Yii::t('ui', 'Competence'), 'url'=>array('/Competence'), 'visible'=>!Yii::app()->user->isGuest),
       array('label'=>Yii::t('ui', 'Framework'), 'url'=>array('/Framework'), 'visible'=>!Yii::app()->user->isGuest),
       array('label'=>Yii::t('ui', 'Levels'), 'url'=>array('/Levels'), 'visible'=>!Yii::app()->user->isGuest),
       array('label'=>Yii::t('ui', 'Sponsor'), 'url'=>array('/Sponsor'), 'visible'=>!Yii::app()->user->isGuest),
       array('label'=>Yii::t('ui', 'User'), 'url'=>array('/User'), 'visible'=>!Yii::app()->user->isGuest),
//       array('label'=>Yii::t('ui', 'UserAccessLevel'), 'url'=>array('/UserAccessLevel'), 'visible'=>!Yii::app()->user->isGuest),
    )
));
?>

<br><br>
<?php
/*
		Yii::app()->controller->redirect(array('/TblBusinessPartner'));
SiteController.php<br>
TblAddressController.php<br>
TblAssignmentContactController.php<br>
TblAssignmentController.php<br>
TblBusinessPartnerController.php<br>
TblBusinessPartnerTypeController.php<br>
TblContactController.php<br>
TblElectronicContactController.php<br>
TblElectronicContactTypesController.php<br>
TblLevelsController.php<br>
TblNoteController.php<br>
TblObjectiveController.php<br>
TblObjectiveTypeController.php<br>
TblUserAccessLevelController.php<br>
TblUserController.php<br>
UserController.php<br>
*/

?>

<br><br>
<p> List of control pages for the website </p>

<?php
$this->breadcrumbs=array(
	'Admin',
);

?>
