<?php

$this->breadcrumbs = array(
	CoachingContract::label(2),
	'Index',
);

$this->menu = array(
	array('label'=>'Create' . ' ' . CoachingContract::label(), 'url' => array('create')),
	array('label'=>'Manage' . ' ' . CoachingContract::label(2), 'url' => array('admin')),
);
?>

<h1><?php echo GxHtml::encode(CoachingContract::label(2)); ?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 