<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('coaching_contract_id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->coaching_contract_id), array('view', 'id' => $data->coaching_contract_id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('client')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->client)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('assignment')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->assignment)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('framework')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->framework)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('coach')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->coach)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sponsor')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->sponsor)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('manager_id')); ?>:
	<?php echo GxHtml::encode($data->manager_id); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('manager_present')); ?>:
	<?php echo GxHtml::encode($data->manager_present); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sponsor_present')); ?>:
	<?php echo GxHtml::encode($data->sponsor_present); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('initial_meeting_date')); ?>:
	<?php echo GxHtml::encode($data->initial_meeting_date); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('number_of_sessions')); ?>:
	<?php echo GxHtml::encode($data->number_of_sessions); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('dataGathering')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->dataGathering)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('review_point')); ?>:
	<?php echo GxHtml::encode($data->review_point); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('record_created')); ?>:
	<?php echo GxHtml::encode($data->record_created); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('record_updated')); ?>:
	<?php echo GxHtml::encode($data->record_updated); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('createdBy')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->createdBy)); ?>
	<br />
	*/ ?>

</div>