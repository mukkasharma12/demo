<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>'List' . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>'Create' . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>'Update' . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->coaching_contract_id)),
	array('label'=>'Delete' . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->coaching_contract_id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage' . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo 'View' . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'coaching_contract_id',
array(
			'name' => 'client',
			'type' => 'raw',
			'value' => $model->client !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->client)), array('client/view', 'id' => GxActiveRecord::extractPkValue($model->client, true))) : null,
			),
array(
			'name' => 'assignment',
			'type' => 'raw',
			'value' => $model->assignment !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->assignment)), array('assignment/view', 'id' => GxActiveRecord::extractPkValue($model->assignment, true))) : null,
			),
array(
			'name' => 'framework',
			'type' => 'raw',
			'value' => $model->framework !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->framework)), array('framework/view', 'id' => GxActiveRecord::extractPkValue($model->framework, true))) : null,
			),
array(
			'name' => 'coach',
			'type' => 'raw',
			'value' => $model->coach !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->coach)), array('coach/view', 'id' => GxActiveRecord::extractPkValue($model->coach, true))) : null,
			),
array(
			'name' => 'sponsor',
			'type' => 'raw',
			'value' => $model->sponsor !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->sponsor)), array('sponsor/view', 'id' => GxActiveRecord::extractPkValue($model->sponsor, true))) : null,
			),
'manager_id',
'manager_present',
'sponsor_present',
'initial_meeting_date',
'number_of_sessions',
array(
			'name' => 'dataGathering',
			'type' => 'raw',
			'value' => $model->dataGathering !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->dataGathering)), array('coachingContractDataGathering/view', 'id' => GxActiveRecord::extractPkValue($model->dataGathering, true))) : null,
			),
'review_point',
'record_created',
'record_updated',
array(
			'name' => 'createdBy',
			'type' => 'raw',
			'value' => $model->createdBy !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->createdBy)), array('user/view', 'id' => GxActiveRecord::extractPkValue($model->createdBy, true))) : null,
			),
	),
)); ?>

<h2><?php echo GxHtml::encode($model->getRelationLabel('coachingContractAreas')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->coachingContractAreas as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('coachingContractArea/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?>