<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'coaching-contract-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		Fields with <span class="required">*</span> are required.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'client_id'); ?>
		<?php echo $form->dropDownList($model, 'client_id', GxHtml::listDataEx(Client::model()->findAllAttributes(null, true)), array('empty'=>'') ); ?>
		<?php echo $form->error($model,'client_id'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'assignment_id'); ?>
		<?php echo $form->dropDownList($model, 'assignment_id', GxHtml::listDataEx(Assignment::model()->findAllAttributes(null, true)), array('empty'=>'') ); ?>
		<?php echo $form->error($model,'assignment_id'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'framework_id'); ?>
		<?php echo $form->dropDownList($model, 'framework_id', GxHtml::listDataEx(Framework::model()->findAllAttributes(null, true)), array('empty'=>'') ); ?>
		<?php echo $form->error($model,'framework_id'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'coach_id'); ?>
		<?php echo $form->dropDownList($model, 'coach_id', GxHtml::listDataEx(Coach::model()->findAllAttributes(null, true)), array('empty'=>'') ); ?>
		<?php echo $form->error($model,'coach_id'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'sponsor_id'); ?>
		<?php echo $form->dropDownList($model, 'sponsor_id', GxHtml::listDataEx(Sponsor::model()->findAllAttributes(null, true)), array('empty'=>'') ); ?>
		<?php echo $form->error($model,'sponsor_id'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'manager_id'); ?>
		<?php echo $form->textField($model, 'manager_id'); ?>
		<?php echo $form->error($model,'manager_id'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'manager_present'); ?>
		<?php echo $form->textField($model, 'manager_present', array('maxlength' => 1)); ?>
		<?php echo $form->error($model,'manager_present'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'sponsor_present'); ?>
		<?php echo $form->textField($model, 'sponsor_present', array('maxlength' => 1)); ?>
		<?php echo $form->error($model,'sponsor_present'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'initial_meeting_date'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'initial_meeting_date',
			'value' => $model->initial_meeting_date,
			'options' => array(
				'showButtonPanel' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
		<?php echo $form->error($model,'initial_meeting_date'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'number_of_sessions'); ?>
		<?php echo $form->textField($model, 'number_of_sessions'); ?>
		<?php echo $form->error($model,'number_of_sessions'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'data_gathering_id'); ?>
		<?php echo $form->dropDownList($model, 'data_gathering_id', GxHtml::listDataEx(CoachingContractDataGathering::model()->findAllAttributes(null, true)), array('empty'=>'') ); ?>
		<?php echo $form->error($model,'data_gathering_id'); ?>
		</div><!-- row -->
			<div class="row">
		<?php echo $form->labelEx($model,'review_point'); ?>
		<?php echo $form->textField($model, 'review_point', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'review_point'); ?>
		</div><!-- row -->
	
 

<?php
echo GxHtml::submitButton('Save');
$this->endWidget();
?>
</div><!-- form -->