<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'coaching_contract_id'); ?>
		<?php echo $form->textField($model, 'coaching_contract_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'client_id'); ?>
		<?php echo $form->dropDownList($model, 'client_id', GxHtml::listDataEx(Client::model()->findAllAttributes(null, true)), array('prompt' => 'All')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'assignment_id'); ?>
		<?php echo $form->dropDownList($model, 'assignment_id', GxHtml::listDataEx(Assignment::model()->findAllAttributes(null, true)), array('prompt' => 'All')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'framework_id'); ?>
		<?php echo $form->dropDownList($model, 'framework_id', GxHtml::listDataEx(Framework::model()->findAllAttributes(null, true)), array('prompt' => 'All')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'coach_id'); ?>
		<?php echo $form->dropDownList($model, 'coach_id', GxHtml::listDataEx(Coach::model()->findAllAttributes(null, true)), array('prompt' => 'All')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'sponsor_id'); ?>
		<?php echo $form->dropDownList($model, 'sponsor_id', GxHtml::listDataEx(Sponsor::model()->findAllAttributes(null, true)), array('prompt' => 'All')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'manager_id'); ?>
		<?php echo $form->textField($model, 'manager_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'manager_present'); ?>
		<?php echo $form->textField($model, 'manager_present', array('maxlength' => 1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'sponsor_present'); ?>
		<?php echo $form->textField($model, 'sponsor_present', array('maxlength' => 1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'initial_meeting_date'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'initial_meeting_date',
			'value' => $model->initial_meeting_date,
			'options' => array(
				'showButtonPanel' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'number_of_sessions'); ?>
		<?php echo $form->textField($model, 'number_of_sessions'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'data_gathering_id'); ?>
		<?php echo $form->dropDownList($model, 'data_gathering_id', GxHtml::listDataEx(CoachingContractDataGathering::model()->findAllAttributes(null, true)), array('prompt' => 'All')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'review_point'); ?>
		<?php echo $form->textField($model, 'review_point', array('maxlength' => 255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'record_created'); ?>
		<?php echo $form->textField($model, 'record_created'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'record_updated'); ?>
		<?php echo $form->textField($model, 'record_updated'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'created_by'); ?>
		<?php echo $form->dropDownList($model, 'created_by', GxHtml::listDataEx(User::model()->findAllAttributes(null, true)), array('prompt' => 'All')); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
